import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.fuksina.tm.client.ProjectRestEndpointClient;
import ru.tsc.fuksina.tm.marker.IntegrationCategory;
import ru.tsc.fuksina.tm.model.Project;

import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    final Project project1 = new Project("project 1");

    @NotNull
    final Project project2 = new Project("project 2");

    @NotNull
    final Project project3 = new Project("project 3");

    private static long INITIAL_SIZE;

    @Before
    public void init() {
        client.save(project1);
        client.save(project2);
        INITIAL_SIZE = client.count();
    }

    @After
    public void end() {
        client.clear();
    }

    @Test
    public void save() {
        @NotNull final String name = project3.getName();
        @NotNull final Project project = client.save(project3);
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(INITIAL_SIZE + 1, client.count());
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = client.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(INITIAL_SIZE, projects.size());
    }

    @Test
    public void findById() {
        @NotNull final String name = project1.getName();
        @NotNull final Project project = client.findById(project1.getId());
        Assert.assertEquals(name, project.getName());
    }

    @Test
    public void existsById() {
        final boolean exists = client.existsById(project1.getId());
        final boolean notExists = client.existsById("123");
        Assert.assertTrue(exists);
        Assert.assertFalse(notExists);
    }

    @Test
    public void delete() {
        client.delete(project2);
        @NotNull final Project project = client.findById(project2.getId());
        Assert.assertNull(project);
    }

    @Test
    public void deleteAll() {
        client.clear(client.findAll());
        @NotNull final List<Project> projects = client.findAll();
        Assert.assertEquals(0, projects.size());
    }

    @Test
    public void clear() {
        client.clear();
        @NotNull final List<Project> projects = client.findAll();
        Assert.assertEquals(0, projects.size());
    }

    @Test
    public void count() {
        @NotNull final List<Project> projects = client.findAll();
        Assert.assertEquals(projects.size(), client.count());
    }

}
