package ru.tsc.fuksina.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.model.Project;
import ru.tsc.fuksina.tm.model.Task;
import ru.tsc.fuksina.tm.service.ProjectService;
import ru.tsc.fuksina.tm.service.TaskService;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @GetMapping("/task/create")
    public String create() {
        taskService.save("New Task " + System.currentTimeMillis());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task") Task task, BindingResult result) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Task task = taskService.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects());
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    private Collection<Project> getProjects() {
        return projectService.findAll();
    }

    public Status[] getStatuses() {
        return Status.values();
    }

}
