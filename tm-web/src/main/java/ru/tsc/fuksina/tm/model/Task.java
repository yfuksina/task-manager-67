package ru.tsc.fuksina.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.DateUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_dt")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateStart;

    @Nullable
    @Column(name = "end_dt")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateEnd;

    @Nullable
    @Column(name = "project_id")
    private String projectId = null;

    public Task(@NotNull final String name) {
        this.name = name;
    }

}
